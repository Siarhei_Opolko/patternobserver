﻿using System;
using Observer.Widgets;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            var newsAggergator = new NewsAggregator();
            var twitterWidget = new TwitterWidget();
            var lentaWidget = new LentaWidget();
            var tvWidget = new TvWidget();

            newsAggergator.NewsChanged += tvWidget.Update;
            newsAggergator.NewsChanged += lentaWidget.Update;
            newsAggergator.NewsChanged += twitterWidget.Update;

            newsAggergator.NewNewsAvaliable();
            Console.WriteLine();
            newsAggergator.NewsChanged -= tvWidget.Update;
            newsAggergator.NewNewsAvaliable();

            Console.ReadLine();
        }
    }
}
