﻿using System;

namespace Observer.Widgets
{
    class TvWidget : IWidget
    {
        private string _tv;

        public void Update(object sender, NewsEventArgs e)
        {
            _tv = e.tv;
            Display();
        }

        public void Display()
        {
            Console.WriteLine("Tv: {0}", _tv);
        }
    }
}
