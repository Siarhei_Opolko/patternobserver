﻿using System;
using System.Collections.Generic;

namespace Observer
{
    public class NewsEventArgs
    {
        public NewsEventArgs(string _twitter, string _tv, string _lenta)
        {
            twitter = _twitter;
            tv = _tv;
            lenta = _lenta;
        }
        public string twitter { get; private set; }
        public string lenta { get; private set; }
        public string tv { get; private set; }
    }

    public delegate void NewsChangedEventHandler(object sender, NewsEventArgs e);
    public class NewsAggregator
    {
        private Random _random;
        public event NewsChangedEventHandler NewsChanged;
        public NewsAggregator()
        {
            _random = new Random();
        }
    
        private string GetTvNews()
        {
            var news = new List<string>
            {
               "News Tv №1",
               "News Tv №2",
               "News Tv №3"
            };

            return news[_random.Next(0, news.Count)];
        }

        private string GetLentaNews()
        {
            var news = new List<string>
            {
               "News Lenta №1",
               "News Lenta №2",
               "News Lenta №3"
            };

            return news[_random.Next(0, news.Count)];
        }

        private string GetTwitterNews()
        {
            var news = new List<string>
            {
               "News Twitter №1",
               "News Twitter №2",
               "News Twitter №3"
            };

            return news[_random.Next(0, news.Count)];
        }

        public void NewNewsAvaliable()
        {
            string twitter = GetTwitterNews();
            string lenta = GetLentaNews();
            string tv = GetTvNews();
            
            if(NewsChanged != null)
                NewsChanged(this, new NewsEventArgs(twitter, tv,lenta));
        }
    }
}
