﻿namespace Observer.Widgets
{
    interface IWidget
    {
        void Display();
    }
}
