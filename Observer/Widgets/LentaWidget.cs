﻿using System;

namespace Observer.Widgets
{
    class LentaWidget :  IWidget
    {
        private string _lenta;
       
        public void Update(object sender , NewsEventArgs e)
        {
            _lenta = e.lenta;
            Display();
        }

        public void Display()
        {
            Console.WriteLine("Lenta: {0}", _lenta);
        }
    }
}
