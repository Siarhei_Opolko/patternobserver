﻿using Observer.Widgets;

namespace Observer
{
    public interface ISubject
    {
        void RegiterObserver(IObserver observer);
        void RemoveObserver(IObserver observer);
        void NotifyObservers();
    }
}
