﻿using System;

namespace Observer.Widgets
{
    class TwitterWidget : IWidget
    {
        private string _twitter;

        public void Update(object sender, NewsEventArgs e)
        {
            _twitter = e.twitter;
            Display();
        }

        public void Display()
        {
            Console.WriteLine("Twitter: {0}", _twitter);
        }
    }
}
